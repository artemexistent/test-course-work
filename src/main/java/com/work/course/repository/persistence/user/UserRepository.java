package com.work.course.repository.persistence.user;

import com.work.course.repository.dao.User;
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends ReactiveNeo4jRepository<User, Long> {

  @Query("MATCH (user:User{userId: $userName}) MATCH (test:Test{translationKey: $testName}) MERGE (user)-[:OWNER]->(test)")
  Mono<Void> addOwnerRelation(String userName, String testName);

  @Query("MATCH (user:User{userId: $userName}) RETURN user")
  Mono<User> findUserByUserId(String userName);

  @Query("MATCH (user:User{userId: $userName}) SET user.email = $email")
  Mono<Void> updateUser(String userName, String email);

  @Query("MATCH (user:User{userId: $userName}) DETACH DELETE user")
  Mono<Void> deleteUser(String userName);

  @Query("CREATE (user:User{userId: $userName, email: $email})")
  Mono<Void> createUser(String userName, String email);
}
