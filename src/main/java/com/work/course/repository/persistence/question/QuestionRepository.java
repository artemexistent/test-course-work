package com.work.course.repository.persistence.question;

import com.work.course.repository.dao.Question;
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.List;

@Repository
public interface QuestionRepository extends ReactiveNeo4jRepository<Question, Long> {

  Mono<Question> findQuestionByQuestion(String question);

  @Query("CREATE(q:Question{question: $question, answer: $answer, variables: $variables}) RETURN q")
  Mono<Question> createQuestion(String question, List<Integer> answer, List<String> variables);

  @Query("MATCH(q:Question{question: $question}) SET q.answer = $answer, q.variables= $variables")
  Mono<Void> updateQuestion(String question, List<Integer> answer, List<String> variables);

  @Query("MATCH(q:Question{question: $question} DETACH DELETE q")
  Mono<Void> deleteQuestion(String question);

}
