package com.work.course.repository.persistence.test;

import com.work.course.repository.dao.Test;
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface TestRepository extends ReactiveNeo4jRepository<Test, Long> {
  Flux<Test> findAllByIsPrivateIsFalse();

  Mono<Test> findTestByTranslationKeyIs(String translationKey);

  @Query("MATCH (test:Test)<-[:OWNER]-(user:User{userId: $userId}) RETURN test")
  Flux<Test> findAllByUser(String userId);

  @Query("CREATE (test:Test{translationKey: $translationKey, isPrivate: $isPrivate})")
  Mono<Void> createTest(String translationKey, Boolean isPrivate);

  @Query("MATCH (test:Test{translationKey: $translationKey}) SET test.isPrivate = $isPrivate")
  Mono<Void> updateTest(String translationKey, Boolean isPrivate);

  @Query("MATCH (test:Test{translationKey: $translationKey}) DETACH DELETE test")
  Mono<Void> deleteTest(String translationKey);

  @Query("MATCH (q:Question{question: $question}) MATCH (test:Test{translationKey: $testName}) MERGE (user)<-[:HAS_QUESTION]-(test)")
  Mono<Void> addHasQuestionRelation(String question, String testName);

}
