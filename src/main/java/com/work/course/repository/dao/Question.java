package com.work.course.repository.dao;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.neo4j.core.schema.DynamicLabels;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@Builder(toBuilder = true)
@Node("Question")
public class Question implements Serializable {

  public static final String QUESTION_LABEL = "Question";

  @Id
  private final Long id;
  @DynamicLabels
  private List<String> labels;

  @Property("question")
  private String question;
  @Property("answer")
  private List<Integer> answer;
  @Property("variables")
  private List<String> variables;
}
