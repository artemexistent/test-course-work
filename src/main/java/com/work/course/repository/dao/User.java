package com.work.course.repository.dao;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.neo4j.core.schema.DynamicLabels;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;

import java.io.Serializable;
import java.util.List;

@Data
@Builder(toBuilder = true)
@Node("User")
public class User implements Serializable {

  public static final String USER_LABEL = "User";

  @Id
  private final Long id;
  @DynamicLabels
  private List<String> labels;
  @Property("userId")
  private String userId;
  @Property("email")
  private String email;
}
