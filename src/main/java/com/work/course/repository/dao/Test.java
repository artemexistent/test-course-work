package com.work.course.repository.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;
import org.springframework.data.neo4j.core.schema.DynamicLabels;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;
import org.springframework.data.neo4j.core.schema.Relationship;

import java.io.Serializable;
import java.util.List;

import static org.springframework.data.neo4j.core.schema.Relationship.Direction.INCOMING;
import static org.springframework.data.neo4j.core.schema.Relationship.Direction.OUTGOING;

@Data
@Builder(toBuilder = true)
@Node("Test")
public class Test implements Serializable {

  public static final String TEST_LABEL = "Test";

  @Id
  private final Long id;
  @DynamicLabels
  private List<String> labels;
  @Property("translationKey")
  private String translationKey;
  @Property("isPrivate")
  private Boolean isPrivate;

  @Singular
  @Relationship(type = "OWNER", direction = INCOMING)
  private List<User> relatedUsers;

  @Singular
  @Relationship(type = "HAS_QUESTION", direction = OUTGOING)
  private List<Question> relatedQuestions;
}
