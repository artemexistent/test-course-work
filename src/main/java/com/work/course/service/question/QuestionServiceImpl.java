package com.work.course.service.question;

import com.work.course.repository.dao.Question;
import com.work.course.repository.persistence.question.QuestionRepository;
import com.work.course.service.exception.EntityNotFoundException;
import com.work.course.web.exception.model.ErrorCodes;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static com.work.course.web.exception.model.ErrorCodes.ENTITY_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class QuestionServiceImpl implements QuestionService {

  private final QuestionRepository questionRepository;

  @Override
  public Flux<Question> getQuestions() {
    return questionRepository.findAll();
  }

  @Override
  public Mono<Void> updateQuestion(Question question) {
    return questionRepository.findQuestionByQuestion(question.getQuestion())
        .flatMap(question1 -> questionRepository.updateQuestion(question.getQuestion(), question.getAnswer(), question.getVariables()))
        .switchIfEmpty(Mono.error(new EntityNotFoundException("Question not found", ENTITY_NOT_FOUND)));
  }

  @Override
  public Mono<Void> deleteQuestion(String questionName) {
    return questionRepository.findQuestionByQuestion(questionName)
        .flatMap(question1 -> questionRepository.deleteQuestion(questionName))
        .switchIfEmpty(Mono.error(new EntityNotFoundException("Question not found", ENTITY_NOT_FOUND)));
  }

  @Override
  public Mono<Question> createQuestion(Question question) {
    return questionRepository.findQuestionByQuestion(question.getQuestion())
        .switchIfEmpty(questionRepository.createQuestion(question.getQuestion(), question.getAnswer(), question.getVariables()));
  }

}
