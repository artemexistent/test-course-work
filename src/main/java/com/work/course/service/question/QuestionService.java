package com.work.course.service.question;

import com.work.course.repository.dao.Question;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface QuestionService {


  Flux<Question> getQuestions();

  Mono<Void> updateQuestion(Question question);

  Mono<Void> deleteQuestion(String question);

  Mono<Question> createQuestion(Question question);
}
