package com.work.course.service.user;

import com.work.course.repository.dao.User;
import com.work.course.repository.persistence.user.UserRepository;
import com.work.course.service.exception.EntityNotFoundException;
import com.work.course.service.exception.ItemAlreadyExistsException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static com.work.course.web.exception.model.ErrorCodes.ENTITY_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;

  @Override
  public Flux<User> getUsers() {
    return userRepository.findAll();
  }

  @Override
  public Mono<User> getUserInfo(String userName) {
    return userRepository.findUserByUserId(userName)
        .switchIfEmpty(Mono.error(new EntityNotFoundException("User not found", ENTITY_NOT_FOUND)));
  }

  @Override
  public Mono<Void> updateUser(User user) {
    return getUserInfo(user.getUserId())
        .flatMap(user1 -> userRepository.updateUser(user.getUserId(), user.getEmail()));
  }

  @Override
  public Mono<Void> deleteUser(String userName) {
    return getUserInfo(userName)
        .flatMap(user -> userRepository.deleteUser(userName));
  }

  @Override
  public Mono<Void> createUser(User user) {
    return userRepository.findUserByUserId(user.getUserId())
        .flatMap(ignore -> Mono.error(new ItemAlreadyExistsException("User already exist")))
        .switchIfEmpty(userRepository.createUser(user.getUserId(), user.getEmail()))
        .then();
  }
}
