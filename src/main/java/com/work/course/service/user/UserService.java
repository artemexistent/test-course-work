package com.work.course.service.user;

import com.work.course.repository.dao.User;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {

  Flux<User> getUsers();

  Mono<User> getUserInfo(String userName);

  Mono<Void> updateUser(User user);

  Mono<Void> deleteUser(String userName);

  Mono<Void> createUser(User user);

}
