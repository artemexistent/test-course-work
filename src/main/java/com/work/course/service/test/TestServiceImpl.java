package com.work.course.service.test;

import com.work.course.repository.dao.Test;
import com.work.course.repository.dao.User;
import com.work.course.repository.persistence.test.TestRepository;
import com.work.course.repository.persistence.user.UserRepository;
import com.work.course.service.exception.EntityNotFoundException;
import com.work.course.service.exception.ItemAlreadyExistsException;
import com.work.course.service.question.QuestionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static com.work.course.web.exception.model.ErrorCodes.ENTITY_NOT_FOUND;

@Service
@Slf4j
@RequiredArgsConstructor
public class TestServiceImpl implements TestService {

  private final TestRepository testRepository;
  private final UserRepository userRepository;
  private final QuestionService questionService;

  @Override
  public Flux<Test> getTests() {
    return testRepository.findAllByIsPrivateIsFalse();
  }

  @Override
  public Mono<Test> getTestsByName(String testName, String userId) {
    return testRepository.findTestByTranslationKeyIs(testName)
        .switchIfEmpty(Mono.error(new EntityNotFoundException("Test not found", ENTITY_NOT_FOUND)))
        .handle((test, sink) -> {
          if (test.getIsPrivate()) {
            if (test.getRelatedUsers().stream().map(User::getUserId).toList().contains(userId)) {
              sink.next(test);
            } else {
              sink.error(new EntityNotFoundException("Test unavailable for user", ENTITY_NOT_FOUND));
            }
          } else {
            sink.next(test);
          }
        });
  }

  @Override
  public Flux<Test> getTestsByUser(String userID) {
    return testRepository.findAllByUser(userID);
  }

  @Override
  public Mono<Void> createTest(Test test, String userId) {
    return testRepository.findTestByTranslationKeyIs(test.getTranslationKey())
        .map(testNode -> Mono.error(new ItemAlreadyExistsException("Test already exists")))
        .switchIfEmpty(testRepository.createTest(test.getTranslationKey(), test.getIsPrivate())
            .then(Mono.just(test.getRelatedQuestions().stream()
                .map(questionService::createQuestion)
                .map(question -> question
                    .map(q -> testRepository.addHasQuestionRelation(q.getQuestion(), test.getTranslationKey()))
                )
                .toList()))
            .then(userRepository.addOwnerRelation(userId, test.getTranslationKey())).then(Mono.empty()))
        .then();
  }

  @Override
  public Mono<Void> updateTest(Test test, String userId) {
    return testRepository.findTestByTranslationKeyIs(test.getTranslationKey())
        .map(testNode -> Mono.error(new ItemAlreadyExistsException("Test already exists")))
        .switchIfEmpty(testRepository.updateTest(test.getTranslationKey(), test.getIsPrivate()).then(Mono.empty()))
        .then();
  }

  @Override
  public Mono<Void> deleteTest(String testName, String userId) {
    return testRepository.findTestByTranslationKeyIs(testName)
        .handle((test, sink) -> {
          if (test.getRelatedUsers().stream().map(User::getUserId).toList().contains(userId)) {
            testRepository.deleteTest(testName);
            sink.complete();
          } else {
            sink.error(new EntityNotFoundException("Test not found", ENTITY_NOT_FOUND));
          }
        });
  }

  @Override
  public Mono<Void> addOwner(String testName, String userId, String ownerId) {
    return testRepository.findTestByTranslationKeyIs(testName)
        .switchIfEmpty(Mono.error(new EntityNotFoundException("Test not found", ENTITY_NOT_FOUND)))
        .handle((test, sink) -> {
          if (test.getIsPrivate()) {
            if (test.getRelatedUsers().stream().map(User::getUserId).toList().contains(ownerId)) {
              sink.next(test);
            } else {
              sink.error(new EntityNotFoundException("Test unavailable for user", ENTITY_NOT_FOUND));
            }
          } else {
            sink.next(test);
          }
        }).then(userRepository.findUserByUserId(userId))
        .switchIfEmpty(Mono.error(new EntityNotFoundException("User not found", ENTITY_NOT_FOUND)))
        .map(user -> userRepository.addOwnerRelation(userId, testName))
        .then();
  }
}
