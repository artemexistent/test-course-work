package com.work.course.service.test;

import com.work.course.repository.dao.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TestService {

  Flux<Test> getTests();

  Mono<Test> getTestsByName(String testName, String userId);

  Flux<Test> getTestsByUser(String userId);

  Mono<Void> createTest(Test test, String userId);

  Mono<Void> updateTest(Test test, String userId);

  Mono<Void> deleteTest(String testName, String userId);

  Mono<Void> addOwner(String testName, String userId, String ownerId);

}
