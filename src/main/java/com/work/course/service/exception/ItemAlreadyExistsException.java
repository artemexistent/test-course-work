package com.work.course.service.exception;

public class ItemAlreadyExistsException extends RuntimeException {
  public ItemAlreadyExistsException(String message) {
    super(message);
  }
}