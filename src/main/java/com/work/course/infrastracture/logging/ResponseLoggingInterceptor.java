package com.work.course.infrastracture.logging;

import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;

import static java.nio.charset.StandardCharsets.UTF_8;

@Slf4j
public class ResponseLoggingInterceptor extends ServerHttpResponseDecorator {

  private final Logger logger;

  public ResponseLoggingInterceptor(ServerHttpResponse response, Logger logger) {
    super(response);
    this.logger = logger;
  }

  @Override
  public Mono<Void> writeWith(Publisher<? extends DataBuffer> body) {
    return super.writeWith(Flux.from(body)
        .publishOn(Schedulers.boundedElastic())
        .doOnNext(dataBuffer -> {
          try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            Channels.newChannel(outputStream).write(dataBuffer.asByteBuffer().asReadOnlyBuffer());
            String stringBody = outputStream.toString(UTF_8);
            logger.logOutgoingResponse(getStatusCode(), super.getHeaders(), stringBody);
          } catch (IOException ex) {
            log.error(ex.getMessage());
          }
        }));
  }
}