package com.work.course.infrastracture.logging;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.ServerWebExchangeDecorator;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.http.HttpHeaders.CONTENT_LENGTH;
import static org.springframework.http.HttpMethod.GET;

@Component
@RequiredArgsConstructor
public class LoggingFilter implements WebFilter {

  private final Logger logger;

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
    ServerHttpRequest request = exchange.getRequest();

    HttpMethod method = request.getMethod();
    List<String> contentLength = request.getHeaders().get(CONTENT_LENGTH);

    if ((nonNull(method) && method.equals(GET)) || isNull(contentLength) || contentLength.get(0).equals("0")) {
      logger.logIncomingRequest(request.getURI(), request.getMethod(), request.getHeaders(), null);
    }

    ServerWebExchangeDecorator exchangeDecorator = new ServerWebExchangeDecorator(exchange) {
      @Override
      public ServerHttpRequest getRequest() {
        return new RequestLoggingInterceptor(super.getRequest(), logger);
      }

      @Override
      public ServerHttpResponse getResponse() {
        return new ResponseLoggingInterceptor(super.getResponse(), logger);
      }
    };

    return chain.filter(exchangeDecorator);
  }

}