package com.work.course.infrastracture.logging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.Arrays;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.joining;

@Slf4j
@Component
public record Logger() {

  private static <T> String getLogMessage(String name, T object) {
    return isNull(object) ? "" : name + ": " + object + "\n";
  }

  public void logIncomingRequest(URI uri, HttpMethod httpMethod, HttpHeaders httpHeaders, Object requestBody) {

    String method = nonNull(httpMethod) ? httpMethod.name() : null;

    String url = uri.getScheme() + "://" + uri.getHost() + uri.getPath();

    String queryParams = nonNull(uri.getQuery()) ? Arrays.toString(uri.getQuery().split("&")) : null;

    String headers = getHeaders(httpHeaders);

    String body = nonNull(requestBody) ? requestBody.toString().replaceAll("(?:\\n)|(?:\\t)", "") : null;

    log.info("INCOMING request\n" +
        getLogMessage("Method", method) +
        getLogMessage("URL", url) +
        getLogMessage("Query params", queryParams) +
        getLogMessage("Request Body", body) +
        getLogMessage("Headers", headers));
  }

  public void logOutgoingResponse(HttpStatus httpStatus, HttpHeaders httpHeaders, Object responseBody) {

    Integer status = nonNull(httpStatus) ? httpStatus.value() : null;

    String headers = getHeaders(httpHeaders);

    String body = nonNull(responseBody) ? responseBody.toString().replaceAll("(?:\\n)|(?:\\t)", "") : null;

    log.info("sending response for INCOMING request\n" +
        getLogMessage("Status code", status) +
        getLogMessage("Response Body", body) +
        getLogMessage("Headers", headers));
  }

  private String getHeaders(HttpHeaders httpHeaders) {
    if (httpHeaders.isEmpty()) {
      return null;
    }

    return httpHeaders.entrySet()
        .stream()
        .map(header -> header.getKey() + ": " + header.getValue())
        .collect(joining("; "));
  }
}