package com.work.course.infrastracture.logging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;

import static java.nio.charset.StandardCharsets.UTF_8;

@Slf4j
public class RequestLoggingInterceptor extends ServerHttpRequestDecorator {

  private final Logger logger;

  public RequestLoggingInterceptor(ServerHttpRequest request, Logger logger) {
    super(request);
    this.logger = logger;
  }

  @Override
  public Flux<DataBuffer> getBody() {
    return super.getBody()
        .publishOn(Schedulers.boundedElastic())
        .doOnNext(dataBuffer -> {
          try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            Channels.newChannel(outputStream).write(dataBuffer.asByteBuffer().asReadOnlyBuffer());
            String body = outputStream.toString(UTF_8);
            logger.logIncomingRequest(super.getURI(), super.getMethod(), super.getHeaders(), body);
          } catch (IOException ex) {
            log.error(ex.getMessage());
          }
        });
  }
}