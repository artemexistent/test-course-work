package com.work.course.infrastracture.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.GenericConverter;
import org.springframework.data.neo4j.core.convert.Neo4jConversions;

import java.util.Collection;

@Configuration
public class Neo4jTypeConverterConfig {
  @Bean
  public Neo4jConversions neo4jConversions(Collection<GenericConverter> genericConverters) {
    return new Neo4jConversions(genericConverters);
  }
}
