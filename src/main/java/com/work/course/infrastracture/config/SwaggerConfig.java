package com.work.course.infrastracture.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import({BeanValidatorPluginsConfiguration.class})
public class SwaggerConfig {

  private final Environment environment;

  public SwaggerConfig(Environment environment) {
    this.environment = environment;
  }


  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title(environment.getProperty("info.app.name"))
        .description("REST API Documentation")
        .version(environment.getProperty("info.app.version"))
        .build();
  }

  @Bean
  public Docket docket() {
    return new Docket(DocumentationType.OAS_30)
        .apiInfo(this.apiInfo())
        .enable(true)
        .select()
        .paths(PathSelectors.any())
        .build();
  }
}