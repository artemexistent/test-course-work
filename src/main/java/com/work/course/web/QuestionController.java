package com.work.course.web;

import com.work.course.service.question.QuestionService;
import com.work.course.web.converter.QuestionConverter;
import com.work.course.web.model.QuestionDto;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v0/questions")
public class QuestionController {

  private final QuestionConverter questionConverter;
  private final QuestionService questionService;

  @GetMapping
  public Flux<QuestionDto> getQuestions() {
    return questionService.getQuestions().map(questionConverter::toDto);
  }

  @PostMapping
  public Mono<Void> addTest(@RequestBody @Validated QuestionDto questionDto) {
    return questionService.createQuestion(questionConverter.toModel(questionDto)).then();
  }

  @PutMapping
  public Mono<Void> updateTest(@RequestBody @Validated(QuestionDto.UpdateGroup.class) QuestionDto questionDto) {
    return questionService.updateQuestion(questionConverter.toModel(questionDto));
  }

  @DeleteMapping("/{testName}")
  public Mono<Void> deleteTest(@PathVariable String testName) {
    return questionService.deleteQuestion(testName);
  }
}
