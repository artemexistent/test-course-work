package com.work.course.web.exception.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Value;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Value(staticConstructor = "of")
public class ErrorResponse {

  String code;
  String message;
  String originCode;
  String originMessage;

  public static ErrorResponse of(String code, String message) {
    return new ErrorResponse(code, message, null, null);
  }

  public static ErrorResponse withOrigin(String code, String message, String originCode, String originMessage) {
    return new ErrorResponse(code, message, originCode, originMessage);
  }
}
