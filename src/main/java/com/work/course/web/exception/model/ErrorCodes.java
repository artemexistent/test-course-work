package com.work.course.web.exception.model;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ErrorCodes {
  public static final String GENERIC = "error.generic";

  public static final String INVALID_PARAMS = "error.invalid-params";
  public static final String INVALID_REQUEST = "error.invalid-request";
  public static final String UNAUTHORIZED = "error.unauthorized";

  public static final String ENTITY_NOT_FOUND = "error.entity-not-found";
  public static final String ITEM_ALREADY_EXIST = "error.item-already-exist";
}
