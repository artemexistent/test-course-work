package com.work.course.web.converter;

import com.work.course.repository.dao.Test;
import com.work.course.web.model.TestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TestConverter implements Converter<Test, TestDto> {

  private final QuestionConverter questionConverter;

  @Override
  public TestDto toDto(Test model) {
    return TestDto.builder()
        .translationKey(model.getTranslationKey())
        .isPrivate(model.getIsPrivate())
        .questions(model.getRelatedQuestions().stream().map(questionConverter::toDto).toList())
        .build();
  }

  @Override
  public Test toModel(TestDto dto) {
    return Test.builder()
        .translationKey(dto.getTranslationKey())
        .isPrivate(dto.getIsPrivate())
        .relatedQuestions(dto.getQuestions().stream().map(questionConverter::toModel).toList())
        .build();
  }

}
