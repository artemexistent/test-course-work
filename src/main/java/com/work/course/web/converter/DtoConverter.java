package com.work.course.web.converter;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public interface DtoConverter<M extends Serializable, D> {

  D toDto(M model);

  /**
   * Converts collection of models into the list of DTOs.
   *
   * @param entities - model collection
   * @return - list of dtos
   */
  default List<D> toDtoList(Collection<M> entities) {
    if (entities == null) {
      return List.of();
    }

    return entities.stream()
        .map(this::toDto)
        .toList();
  }
}
