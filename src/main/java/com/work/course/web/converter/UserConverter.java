package com.work.course.web.converter;

import com.work.course.repository.dao.Question;
import com.work.course.repository.dao.User;
import com.work.course.web.model.QuestionDto;
import com.work.course.web.model.UserDto;
import org.springframework.stereotype.Component;

@Component
public class UserConverter implements Converter<User, UserDto> {

  @Override
  public UserDto toDto(User model) {
    return UserDto.builder()
        .userId(model.getUserId())
        .userEmail(model.getEmail())
        .build();
  }

  @Override
  public User toModel(UserDto dto) {
    return User.builder()
        .userId(dto.getUserId())
        .email(dto.getUserEmail())
        .build();
  }

}
