package com.work.course.web.converter;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public interface ModelConverter<M extends Serializable, D> {

  M toModel(D dto);

  /**
   * Converts collection of DTOs instances into the list of models.
   *
   * @param dtos - dto collection
   * @return - list of models
   */
  default Set<M> toModelSet(Collection<D> dtos) {
    if (dtos == null) {
      return Set.of();
    }

    return dtos.stream()
        .map(this::toModel)
        .collect(Collectors.toSet());
  }
}
