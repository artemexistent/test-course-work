package com.work.course.web.converter;

import java.io.Serializable;

public interface Converter<M extends Serializable, D> extends ModelConverter<M, D>, DtoConverter<M, D> {

}
