package com.work.course.web.converter;

import com.work.course.repository.dao.Question;
import com.work.course.repository.dao.Test;
import com.work.course.web.model.QuestionDto;
import com.work.course.web.model.TestDto;
import org.springframework.stereotype.Component;

@Component
public class QuestionConverter implements Converter<Question, QuestionDto> {

  @Override
  public QuestionDto toDto(Question model) {
    return QuestionDto.builder()
        .question(model.getQuestion())
        .answer(model.getAnswer())
        .variables(model.getVariables())
        .build();
  }

  @Override
  public Question toModel(QuestionDto dto) {
    return Question.builder()
        .question(dto.getQuestion())
        .answer(dto.getAnswer())
        .variables(dto.getVariables())
        .build();
  }

}
