package com.work.course.web;

import com.work.course.service.test.TestService;
import com.work.course.web.converter.TestConverter;
import com.work.course.web.model.TestDto;
import com.work.course.web.model.TestDto.UpdateGroup;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.security.Principal;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v0/tests")
public class TestController implements UserInformationSecurityContextHolder {

  private final TestConverter testConverter;
  private final TestService testService;

  @GetMapping
  public Flux<TestDto> getTests() {
    return testService.getTests().map(testConverter::toDto);
  }

  @GetMapping("/{testName}")
  public Mono<TestDto> getTestsByName(@PathVariable String testName, Principal principal) {
    return testService.getTestsByName(testName, getUserName(principal)).map(testConverter::toDto);
  }

  @GetMapping("/own")
  public Flux<TestDto> getTestsOwn(Principal principal) {
    return testService.getTestsByUser(getUserName(principal)).map(testConverter::toDto);
  }

  @PostMapping
  public Mono<Void> addTest(@RequestBody @Validated TestDto testDto, Principal principal) {
    return testService.createTest(testConverter.toModel(testDto), getUserName(principal));
  }

  @PutMapping
  public Mono<Void> updateTest(@RequestBody @Validated(UpdateGroup.class) TestDto testDto, Principal principal) {
    return testService.updateTest(testConverter.toModel(testDto), getUserName(principal));
  }

  @DeleteMapping("/{testName}")
  public Mono<Void> deleteTest(@PathVariable String testName, Principal principal) {
    return testService.deleteTest(testName, getUserName(principal));
  }

  @PatchMapping("/{testName}/{userId}")
  public Mono<Void> addOwner(@PathVariable String userId, @PathVariable String testName, Principal principal) {
    return testService.addOwner(testName, userId, getUserName(principal));
  }
}
