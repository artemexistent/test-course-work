package com.work.course.web;


import com.work.course.service.exception.EntityNotFoundException;
import com.work.course.web.exception.model.EnrichedErrorResponse;
import com.work.course.web.exception.model.ErrorResponse;
import io.netty.handler.timeout.ReadTimeoutException;
import io.netty.handler.timeout.WriteTimeoutException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.annotation.Order;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.codec.HttpMessageWriter;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.reactive.function.server.HandlerStrategies;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.reactive.result.view.ViewResolver;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebExceptionHandler;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.work.course.web.exception.model.ErrorCodes.ENTITY_NOT_FOUND;
import static com.work.course.web.exception.model.ErrorCodes.GENERIC;
import static com.work.course.web.exception.model.ErrorCodes.INVALID_PARAMS;
import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Slf4j
@Configuration
@RequiredArgsConstructor
@Order(HIGHEST_PRECEDENCE)
public class GlobalErrorWebExceptionHandler implements WebExceptionHandler {

  @Override
  public Mono<Void> handle(ServerWebExchange exchange, Throwable exception) {
    return handleException(exchange, exception).flatMap(response ->
        response.writeTo(exchange, new ServerResponse.Context() {
          @Override
          public List<HttpMessageWriter<?>> messageWriters() {
            return HandlerStrategies.withDefaults().messageWriters();
          }

          @Override
          public List<ViewResolver> viewResolvers() {
            return HandlerStrategies.withDefaults().viewResolvers();
          }
        })
    );
  }

  private Mono<ServerResponse> handleException(ServerWebExchange exchange, Throwable throwable) {
    final HttpHeaders responseHeaders = exchange.getResponse().getHeaders();
    log.error("ERROR:", throwable);
    // DOMAIN EXCEPTIONS
    if (throwable instanceof EntityNotFoundException ex) {
      return createResponse(NOT_FOUND, ErrorResponse.of(ex.getErrorCode(), throwable.getMessage()), responseHeaders);
    } else if (throwable instanceof InvalidDataAccessResourceUsageException) {
      String message = throwable.getMessage();
      return createResponse(INTERNAL_SERVER_ERROR, ErrorResponse.of(ENTITY_NOT_FOUND, throwable.getMessage()), responseHeaders);

    } else if (throwable instanceof ReadTimeoutException) {
      return createResponse(INTERNAL_SERVER_ERROR, ErrorResponse.of(GENERIC, "Read Timeout Exception"), responseHeaders);
    } else if (throwable instanceof WriteTimeoutException) {
      return createResponse(INTERNAL_SERVER_ERROR, ErrorResponse.of(GENERIC, "Write Timeout Exception"), responseHeaders);
    } else if (throwable instanceof final WebExchangeBindException bindException) {
      final String validationErrorMsg = bindException.getFieldErrors().stream()
          .map(fieldError -> String.format("%s: %s", fieldError.getField(), fieldError.getDefaultMessage()))
          .collect(Collectors.joining(","));

      final Map<String, List<String>> errorDetails = getErrorDetails(bindException);
      final EnrichedErrorResponse enrichedErrorResponse = EnrichedErrorResponse.builder()
          .code(INVALID_PARAMS)
          .message(validationErrorMsg)
          .validation(errorDetails)
          .build();

      return createResponse(BAD_REQUEST, enrichedErrorResponse, responseHeaders);

    }

    // OTHER
    return createResponse(INTERNAL_SERVER_ERROR, ErrorResponse.of(GENERIC, throwable.getMessage()), responseHeaders);
  }

  private Map<String, List<String>> getErrorDetails(WebExchangeBindException bindException) {
    return bindException.getFieldErrors().stream()
        .collect(Collectors.groupingBy(
            FieldError::getField,
            Collectors.mapping(DefaultMessageSourceResolvable::getDefaultMessage, Collectors.toUnmodifiableList())));
  }

  private Mono<ServerResponse> createResponse(HttpStatus httpStatus, Object responseBody, HttpHeaders responseHeaders) {
    return ServerResponse.status(httpStatus).bodyValue(responseBody);
  }

  private Mono<ServerResponse> createResponse(int httpStatus, Object responseBody, HttpHeaders responseHeaders) {
    return ServerResponse.status(httpStatus).bodyValue(responseBody);
  }

}
