package com.work.course.web;

import com.work.course.service.user.UserService;
import com.work.course.web.converter.UserConverter;
import com.work.course.web.model.UserDto.UpdateGroup;
import com.work.course.web.model.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v0/users")
public class UserController {

  private final UserConverter userConverter;
  private final UserService userService;

  @GetMapping
  @PreAuthorize("hasRole('admin')")
  public Flux<UserDto> getUsers() {
    return userService.getUsers().map(userConverter::toDto);
  }

  @GetMapping("/{userName}")
  @PreAuthorize("hasRole('admin')")
  public Mono<UserDto> getUserFullInformation(@PathVariable String userName) {
    return userService.getUserInfo(userName).map(userConverter::toDto);
  }

  @PostMapping
  @PreAuthorize("hasRole('admin')")
  public Mono<Void> addUser(@RequestBody @Validated UserDto userDto) {
    return userService.createUser(userConverter.toModel(userDto));
  }

  @PutMapping
  @PreAuthorize("hasRole('admin')")
  public Mono<Void> updateUser(@RequestBody @Validated(UpdateGroup.class) UserDto userDto) {
    return userService.updateUser(userConverter.toModel(userDto));
  }

  @DeleteMapping("/{userName}")
  public Mono<Void> deleteUser(@PathVariable String userName) {
    return userService.deleteUser(userName);
  }
}
