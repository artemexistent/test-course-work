package com.work.course.web.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
public class UserDto {
  @NotBlank(groups = UpdateGroup.class)
  String userId;
  @NotNull(groups = UpdateGroup.class)
  String userEmail;

  public interface UpdateGroup{}
}
