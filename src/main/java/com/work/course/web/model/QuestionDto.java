package com.work.course.web.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
public class QuestionDto {
  @NotBlank(groups = UpdateGroup.class)
  String question;
  @NotNull
  @NotEmpty
  List<Integer> answer;
  @NotNull
  @NotEmpty
  List<String> variables;

  public interface UpdateGroup{}
}
