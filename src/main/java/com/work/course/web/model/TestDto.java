package com.work.course.web.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
public class TestDto {
  @NotBlank(groups = UpdateGroup.class)
  String translationKey;
  @NotNull(groups = UpdateGroup.class)
  Boolean isPrivate;
  @NotNull
  @NotEmpty
  List<@Valid QuestionDto> questions;

  public interface UpdateGroup{}
}
